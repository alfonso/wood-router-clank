# Wood Router Clank 
<img src="img/portada.png" width=900px >



2022 HTMAA machine week had an interesting take off I definitely enjoyed. Instead students making machines, this time, TAs were the one asked to have some fun making some machines and I personally took it as a dive in into Jake Read's research. It has been a fun activity that allowed me learn how to make my 1st wood router and ideas for how to keep improving it.

<img src="img/6.jpeg" width=800px >


Jake's project [**Clank**](https://gitlab.cba.mit.edu/jakeread/clank) offers open source hardware and software to built machines in various sizes only using extruded aluminum, bearings, and 3D printed parts. A recent iteration, [Clank stretch](https://github.com/jakeread/clank-stretch/) offers a larger size machine and a new  hot plate for interchangeable end effectors that led me to design some more fun attachments to the machine to convert the wood router into a pen plotter veeeeery easily. I also made some modifications that I will go a little bit deeper to simplify the machine a bit. I built a makita adaptor that you can [download](https://gitlab.cba.mit.edu/alfonso/wood-router-clank/-/tree/main/CAD) and use with your Clank hot plate.

I decided I wanted to build a Shapeoko-Carbide dektop router (650mm by 650mm). I showed Jake on Amazon this Makita wood router [Makita RT0700C](https://www.google.com/search?q=Makita+RT0700C&rlz=1C5CHFA_enES881US883&oq=Makita+RT0700C&aqs=chrome..69i57j0i512l6j69i60.445j0j7&sourceid=chrome&ie=UTF-8) able to spin up to 25k rpm and weights around 4 pounds. Seemed that the route might be touching the limits of the machine but worth the try.



Besides, my ultimate goal was to take it with me to hometown, Caniles, in my Christmas luggage. I designed everything around the maximum diagonal able to stuck in my biggest luggage and, spoiler, it somehow worked out.

<img src="img/luggage.png" width=600px >


I downloaded the baseline design and start modifying it. First, as we were worried about the stiffness of the gantry once we assemble the router (a 4kg cantilever end effector), I decided to change the Y-rails from a 20 by 20 aluminum beam into a 60 by 20 aluminum beam. That implied to redesign y-carriers, and the machine legs.

Secondly I also simplified the hot plate mount. On jakes version he made a servo locker that automatically makes the end effector to sit kinematically onto the plate. I removed the servo and added a manual level arm that allows the user to just do it manually. As the user is my father, and, as I knew that he just want a wood router, the simplest, the better. I designed a sacrifice bed and was ready to start assembling it.

The board I used to control the motors is a [TinyGv8](https://github.com/synthetos/TinyG) sending gcode through [Universal Gcode Sender](https://winder.github.io/ugs_website/) (Im in love with this open-source program). I used Fusion360 Manufacturing to generate the Gcode as it allows to use TinyG post processing.

I transported it to Spain. My luggage weighted 78 pounds, and, as a miracle, I was not asked to pay any extra weight. Unbelievable. I arrived home, made a table for the router and assemble it. The first experiments were great. I mainly worked with soft wood (pine) unsing a double flute 6mm diam end mill. But I have to say that I was making a big big mess every time I mill wood. So I decided to design a tiny holder to attach my vacuum machine from my homes woodshop. As a result, the cutest vacuum station Ive seen.

(Before the vacuum station I would need help) <img src="img/alfonsillillo.png" width=600px>






(After the vacuum station no child was needed)
<img src="img/vacuum.gif" width=600px >



After **many** attempts, I can say that the best cutting conditions for soft wood using this machine are:
- RPM 13k
- Feed 1000 - 1200mm/min
- Optimal Load around 1-2mm.

I built multiple pieces, but in order to ensure repeatability I designed a box as a gift for my sister. The box is filled with 30 pens, composed by a top case, lower case and separator. The cases encapsulate the separator and they close snapping each other. That require to mill double face pieces (using register marks to later match the job x0 y0 z0) and multiple pieces with matching geometries, concave and convex.

Through the process I developed this vacuum adaptor for my vacuum machine and it really cleaned the process.

I detoured a bit and, using a Creality Ender3 Pro I designed and built this pen end effector.

<img src="img/pen_end_effector.png" width=800px >

<img src="img/draw.gif" width=300px >


I re-railed into the box and the results were very satisfying. Here are some pics.

<img src="img/1.jpeg" width=800px >
<img src="img/2.jpeg" width=800px >
<img src="img/3.jpeg" width=800px >
<img src="img/4.jpeg" width=800px >
<img src="img/5.jpeg" width=800px >
<img src="img/7.jpeg" width=800px >
<img src="img/8.jpeg" width=800px >
<img src="img/9.jpeg" width=800px >
